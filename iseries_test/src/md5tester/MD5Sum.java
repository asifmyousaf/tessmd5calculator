package md5tester;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MD5Sum {
	
	public static String formateMD5Sum(byte [] messageDigestMD5){
		
		StringBuffer stringBuffer = new StringBuffer();
		
		for (byte bytes : messageDigestMD5) {
			   stringBuffer.append(String.format("%02x", bytes));
		}
		
		return stringBuffer.toString();
	}

	public static Map<String, String> computeMD5(ResultSet rs) throws SQLException, NoSuchAlgorithmException{
		
		HashMap<String, String> md5sumMap = new HashMap<String, String>();
		
		while(rs.next()){
			int columnCount = rs.getMetaData().getColumnCount();
			
			MessageDigest md = MessageDigest.getInstance("MD5");
			StringBuilder strBuilder = new StringBuilder();
			
			for(int i=1; i<=columnCount; i++){
				String content = rs.getString(i);
				md.update(content.getBytes());
				strBuilder.append(content + "\t");
			}
			
			String formattedStr = formateMD5Sum(md.digest());
			md5sumMap.put(strBuilder.toString(), formattedStr );
		}
		return md5sumMap;
	}
}
