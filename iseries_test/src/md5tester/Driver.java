package md5tester;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

public class Driver {

	public static Connection getConnection(String jdbcURL, String username, String password) throws SQLException {

		try {
			// Load the IBM Data Server Driver for JDBC and SQLJ with
			// DriverManager
			Class.forName("com.ibm.as400.access.AS400JDBCDriver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

		Connection con = DriverManager.getConnection(jdbcURL, username, password);

		return con;

	}

	private static String usage() {
		String usage = "java -jar tesstest.jar [connection_url] [username] [password]";
		return usage;
	}

	public static void printMD5Sums(Map<String, String> md5Sums) {
		Iterator it = md5Sums.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry pair = (Map.Entry) it.next();
			System.out.println(pair.getKey() + " = " + pair.getValue());
			it.remove(); // avoids a ConcurrentModificationException
		}
	}

	public static void main(String... args) throws SQLException, NoSuchAlgorithmException {
		
		Connection connection = null;
		
		if (args.length == 0) {
			System.out.println(usage());
		} else {
			connection = getConnection(args[0], args[1], args[2]);
			System.out.println("Enter query : ");
		}
		try {
			BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
			String s = "test";
			while (!s.equals("quit")) {
				s = bufferRead.readLine();

				Statement stmt = null;
				stmt = connection.createStatement();
				ResultSet rs = stmt.executeQuery(s);

				Map<String, String> md5Sums = MD5Sum.computeMD5(rs);

				printMD5Sums(md5Sums);

				System.out.println();
				System.out.println("Enter query : ");
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
